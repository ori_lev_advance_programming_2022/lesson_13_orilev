#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <mutex>
#include <queue>
#include <vector>

#include "SocketData.h"


class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	void acceptClient();
	void clientHandler(SOCKET clientSocket);

	void serverProcessor();
	std::string getAllUsers();


	SOCKET _serverSocket;
	// queue sync
	std::mutex _queueLock;
	std::condition_variable _queueSync;
	std::queue<SocketData> _messagesQueue;
	std::vector<std::string> _users;
};

