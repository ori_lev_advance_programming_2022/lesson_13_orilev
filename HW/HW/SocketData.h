#pragma once

#include <WinSock2.h>
#include <string>


class SocketData {
public:
	SocketData(int code, std::string name); // login/exit packet
	SocketData(int code, std::string name, std::string secondName, std::string message, SOCKET sock); // general packet

	// getters
	int getCode() const;
	std::string getUserName() const;
	std::string getSecondUserName() const;
	std::string getMessage() const;
	SOCKET getSocket() const;

private:
	int _code;
	std::string _userName;
	std::string _secondUserName;
	std::string _message;
	SOCKET _sock;
};