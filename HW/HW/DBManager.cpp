#include "DBManager.h"

#include <fstream>

/*
* function write to the caht file the new message
* input: SocketData Object
* output: none
*/
void DBManager::writeData(const SocketData& data)
{
	std::ofstream file;
	
	// append or create.
	file.open(getFileName(data), std::ios::out | std::ios::app);
	 
	if (file.fail())
		throw std::exception();
	
	const std::string msg =  "&MAGSH_MESSAGE&&Author&" + data.getUserName() + "&DATA&" + data.getMessage();

	file.write(msg.c_str(), msg.length());
	file.close();
}


/*
* function get SocketData between two users and return their chat.
* input: SocketData Object
* output: file content
*/
std::string DBManager::readData(const SocketData& data)
{
	const std::string filePath = getFileName(data);
	std::string content = "";
	std::ifstream f;

	if (!isFileExists(data))
		return content;

	f.open(filePath);
	f >> content;
	f.close();

	return content;
}



/*
* function get DataSocket and return a name of the file who shared between the users
* input: SocketData Object
* output: file name
*/
std::string DBManager::getFileName(const SocketData& data)
{
	std::string firstUser = data.getUserName();
	std::string secondUser = data.getSecondUserName();

	if (firstUser < secondUser)
		return firstUser + "&" + secondUser + ".txt";
	return secondUser + "&" + firstUser + ".txt";
}


/*
* function check if the file exist
* input: SocketData Object
* output: true - exists, else - false
*/
bool DBManager::isFileExists(const SocketData& data)
{
	std::string filePath = getFileName(data);
	std::ifstream ifile;

	ifile.open(filePath);

	if (ifile.is_open()) {
		ifile.close();
		return true;
	}
	return false;
}
