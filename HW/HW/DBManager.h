#pragma once

#include "SocketData.h"

#include <string>

class DBManager
{
public:
	void writeData(const SocketData& data);
	std::string readData(const SocketData& data);
	
private:
	std::string getFileName(const SocketData& data);
	bool isFileExists(const SocketData& data);
};