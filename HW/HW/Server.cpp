#include "Server.h"
#include "Helper.h"
#include "DBManager.h"

#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include <fstream>

#define USER_NAME_BYTES_LENGTH 2
#define MESSAGE_BYTES_LENGHT 5

Server::Server()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}


/*
* function is the dardas aba thread, listen to new clients and accept them\
* input: port number
* output: none
*/
void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	// start the thread who treat the messages
	std::thread t(&Server::serverProcessor, this);
	t.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		acceptClient();
	}
}


void Server::acceptClient()
{
	// this accepts the client and create a specific socket from server to this client
	// the process will not continue until a client connects to the server
	SOCKET client_socket = accept(_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	
	// create a new thread that handle the client, this thread return to listen
	std::thread t(&Server::clientHandler, this, client_socket);
	t.detach();
}


/*
* function handle the client
* input: client socket
* output: none
*/
void Server::clientHandler(SOCKET clientSocket)
{
	int code = 0, size = 0;
	std::string name = "", secondName = "", message = "";

	try
	{
		// login session, need to get his name
		code = Helper::getMessageTypeCode(clientSocket);

		if (code == MT_CLIENT_LOG_IN)
		{
			size = Helper::getIntPartFromSocket(clientSocket, USER_NAME_BYTES_LENGTH);
			name = Helper::getStringPartFromSocket(clientSocket, size);

			SocketData sd = SocketData(code, name, "", "", clientSocket);

			std::lock_guard<std::mutex> l(_queueLock);
			_messagesQueue.push(sd);
		}
		else
			throw std::exception();
		
		_queueSync.notify_one();

		// communication session
		while (true)
		{
			code = Helper::getMessageTypeCode(clientSocket);

			if(code == 0)
				throw std::exception();

			// get the second name
			size = Helper::getIntPartFromSocket(clientSocket, USER_NAME_BYTES_LENGTH);
			secondName = Helper::getStringPartFromSocket(clientSocket, size);

			// get the message
			size = Helper::getIntPartFromSocket(clientSocket, MESSAGE_BYTES_LENGHT);
			message = Helper::getStringPartFromSocket(clientSocket, size);

			SocketData sd = SocketData(code, name, secondName, message, clientSocket);
			{
				std::lock_guard<std::mutex> l(_queueLock);
				_messagesQueue.push(sd);
			}
			_queueSync.notify_one();
		}
	}
	catch (...)
	{
		std::cout << "CATCH ERROR ON HANDLE CLIENT " << name << std::endl;
		SocketData sd = SocketData(MT_CLIENT_EXIT, name);
		{
			std::lock_guard<std::mutex> l(_queueLock);
			_messagesQueue.push(sd);
		}
		_queueSync.notify_one();

		closesocket(clientSocket);
		return;
	}
}

/*
* function get tasks from the shared queue (read from database, write to database, send response) and handle them.
* input: none
* output: none
*/
void Server::serverProcessor()
{
	std::string chat = "";
	DBManager manager = DBManager();

	SOCKET sock;


	while (true)
	{
		std::unique_lock<std::mutex> lock(_queueLock);
		_queueSync.wait(lock);

		SocketData sd = _messagesQueue.front();
		_messagesQueue.pop();
		lock.unlock(); // prevent deadlock, lock the queue only when i needded
		std::cout << "CODE: " << sd.getCode() << "  " << sd.getUserName() << std::endl;
		sock = sd.getSocket();

		try {
			switch (sd.getCode())
			{
			case MT_CLIENT_LOG_IN:
				_users.push_back(sd.getUserName());
				Helper::send_update_message_to_client(sock, "", "", getAllUsers());
				break;


			case MT_CLIENT_UPDATE:
				if (sd.getSecondUserName() != "" && sd.getMessage() != "")
					manager.writeData(sd);
				
				if(sd.getSecondUserName() != "")
					chat = manager.readData(sd);
				
				Helper::send_update_message_to_client(sock, chat, sd.getSecondUserName(), getAllUsers());
				break;


			case MT_CLIENT_FINISH:
			case MT_CLIENT_EXIT:
				_users.erase(std::remove(_users.begin(), _users.end(), sd.getUserName()), _users.end());
				std::cout << "CURRENT LIST:" << getAllUsers() << std::endl;
				break;

			default:
				std::cerr << "[*] ERROR: Client " << sd.getUserName() << " sent code: " << sd.getCode()<< std::endl;
				break;
			}
		}
		catch (...)
		{
			std::cout << "[*] CLOSE SOCKET: " << sd.getUserName() << std::endl;
		}
	}
}


/*
* function return all the connected users, with "&" between them
* input: none
* output: list of users
*/
std::string Server::getAllUsers()
{
	std::string allUsers = "";

	for (const auto& user : _users)
		allUsers += user + "&";

	if(allUsers.size() != 0)
		allUsers.pop_back(); // remove the last "&"

	return allUsers;
}

