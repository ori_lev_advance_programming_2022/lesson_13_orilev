#include "SocketData.h"


SocketData::SocketData(int code, std::string name)
{
    _code = code;
    _userName = name;

    // prevent unexpected things
    _secondUserName = "";
    _message = "";
    _sock = 0;
}

SocketData::SocketData(int code, std::string name, std::string secondName, std::string message, SOCKET sock)
{
    _code = code;
    _userName = name;
    _secondUserName = secondName;
    _message = message;
    _sock = sock;
}

int SocketData::getCode() const
{
    return _code;
}

std::string SocketData::getUserName() const
{
    return _userName;
}

std::string SocketData::getSecondUserName() const
{
    return _secondUserName;
}

std::string SocketData::getMessage() const
{
    return _message;
}

SOCKET SocketData::getSocket() const
{
    return _sock;;
}
